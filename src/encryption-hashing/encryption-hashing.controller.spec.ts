import { Test, TestingModule } from '@nestjs/testing';
import { EncryptionHashingController } from './encryption-hashing.controller';

describe('EncriptionHashingController', () => {
  let controller: EncryptionHashingController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EncryptionHashingController],
    }).compile();

    controller = module.get<EncryptionHashingController>(EncryptionHashingController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
