import { Module } from '@nestjs/common';
import { EncryptionHashingService } from './encryption-hashing.service';
import { EncryptionHashingController } from './encryption-hashing.controller';

@Module({
  providers: [EncryptionHashingService],
  controllers: [EncryptionHashingController],
  exports: [EncryptionHashingService],
})
export class EncryptionHashingModule {}
