import {Body, Controller, Post, UseGuards} from '@nestjs/common';
import {Roles} from "../auth/roles.decorator";
import {Role} from "../enums/role.enum";
import {JwtAuthGuard} from "../auth/jwt-auth.guard";
import {RolesGuard} from "../auth/roles.guard";
import {EncryptionHashingService} from "./encryption-hashing.service";
import {TextBody} from "./text.body";
import {HashBody} from "./hash.body";

@UseGuards(JwtAuthGuard, RolesGuard)
@Controller()
export class EncryptionHashingController {
    constructor(private encryptionHashingService: EncryptionHashingService) {
    }

    @Post("encrypt")
    @Roles(Role.User)
    async encrypt(@Body() textBody: TextBody) {
        const buffer = await this.encryptionHashingService.encrypt(textBody.text)
        return buffer.toString('base64')
    }

    @Post("decrypt")
    @Roles(Role.User)
    async decrypt(@Body() textBody: TextBody) {
        const text = await this.encryptionHashingService.decrypt(Buffer.from(textBody.text, 'base64'))
        return text.toString()
    }

    @Post("hash")
    @Roles(Role.User)
    async generateHash(@Body() textBody: TextBody) {
        return this.encryptionHashingService.generateHash(textBody.text)
    }

    @Post("verify")
    @Roles(Role.User)
    async verifyHash(@Body() hashBody: HashBody) {
        return this.encryptionHashingService.verifyHash(hashBody.password, hashBody.hash)
    }
}
