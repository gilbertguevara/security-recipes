import { Test, TestingModule } from '@nestjs/testing';
import { EncryptionHashingService } from './encryption-hashing.service';

describe('EncriptionHashingService', () => {
  let service: EncryptionHashingService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EncryptionHashingService],
    }).compile();

    service = module.get<EncryptionHashingService>(EncryptionHashingService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
