import { Injectable } from '@nestjs/common';
import {createCipheriv, createDecipheriv, randomBytes, scrypt} from "crypto";
import {promisify} from "util";
import * as bcrypt from 'bcrypt';

@Injectable()
export class EncryptionHashingService {
    private readonly iv = randomBytes(16);
    private readonly password = 'Password';
    private key: Buffer
    private saltOrRounds = 10;

    async encrypt(text: string) {
        this.key = (await promisify(scrypt)(this.password, 'salt', 32)) as Buffer;
        const cipher = createCipheriv('aes-256-ctr', this.key, this.iv);
        return Buffer.concat([
            cipher.update(text),
            cipher.final(),
        ]);
    }

    async decrypt(encryptedText: Buffer) {
        this.key = (await promisify(scrypt)(this.password, 'salt', 32)) as Buffer;
        const decipher = createDecipheriv('aes-256-ctr', this.key, this.iv);
        return Buffer.concat([
            decipher.update(encryptedText),
            decipher.final(),
        ]);
    }

    async generateHash(password: string) {
        return bcrypt.hash(password, this.saltOrRounds);
    }

    async verifyHash(password: string, hash: string) {
        return bcrypt.compare(password, hash);
    }
}
