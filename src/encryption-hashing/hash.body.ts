export interface HashBody {
    password: string,
    hash: string
}