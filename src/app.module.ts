import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ArticleModule } from './article/article.module';
import { EncryptionHashingModule } from './encryption-hashing/encryption-hashing.module';

@Module({
  imports: [AuthModule, UsersModule, ArticleModule, EncryptionHashingModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
