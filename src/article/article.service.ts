import {BadRequestException, Injectable} from '@nestjs/common';

export interface Article {
    id: number,
    title: string,
    content: string,
}

@Injectable()
export class ArticleService {
    private articles = [
        {
            id: 1,
            title: 'Authentication',
            content: 'It\'s an essential part of any application.',
        },
        {
            id: 2,
            title: 'Authorization',
            content: 'Refers to the process that determines what a user is able to do.',
        },
    ];

    async findOne(id: number): Promise<Article | undefined> {
        return this.articles.find(article => article.id === id);
    }

    async findAll(): Promise<Article[]> {
        return this.articles;
    }

    async save(post: Article): Promise<void> {
        this.articles.push(post)
    }

    async update(post: Article): Promise<void> {
        const index = this.articles.findIndex((existingArticle, index) => post.id === existingArticle.id);
        if (index < 0) {
            throw new BadRequestException(`Non-existing article: ${post.id}`)
        }
        this.articles[index] = post;
    }

    async delete(id: number): Promise<void> {
        const index = this.articles.findIndex((existingArticle, index) => id === existingArticle.id);
        if (index < 0) {
            throw new BadRequestException(`Non-existing article: ${id}`)
        }
        this.articles.splice(index, 1);
    }
}
