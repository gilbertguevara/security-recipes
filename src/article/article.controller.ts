import {Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UseGuards} from '@nestjs/common';
import {Article, ArticleService} from "./article.service";
import {JwtAuthGuard} from "../auth/jwt-auth.guard";
import {Role} from "../enums/role.enum";
import {Roles} from "../auth/roles.decorator";
import {RolesGuard} from "../auth/roles.guard";
import {PermissionsGuard} from "../auth/permissions.guard";
import {Permission} from "../enums/permission.enum";
import {Permissions} from "../auth/permissions.decorator";

@UseGuards(JwtAuthGuard, RolesGuard, PermissionsGuard)
@Controller('article')
export class ArticleController {
    constructor(private articleService: ArticleService) {}

    @Get()
    @Roles(Role.User, Role.Author)
    async findAll(): Promise<Article[]> {
        return this.articleService.findAll();
    }

    @Post()
    @HttpCode(204)
    @Roles(Role.Author)
    async create(@Body() article: Article) {
        return this.articleService.save(article);
    }

    @Put(':id')
    @HttpCode(204)
    @Roles(Role.Author)
    async update(@Body() article: Article, @Param('id') id: string) {
        const articleToUpdate = {id: +id, title: article.title, content: article.content};
        return this.articleService.update(articleToUpdate);
    }

    @Delete(':id')
    @HttpCode(204)
    @Permissions(Permission.DeleteArticle)
    async delete(@Param('id') id: string) {
        return this.articleService.delete(+id);
    }

    @Get(':id')
    @Roles(Role.User, Role.Author)
    async findOne(@Param('id') id: string): Promise<Article> {
        return this.articleService.findOne(+id)
    }
}
