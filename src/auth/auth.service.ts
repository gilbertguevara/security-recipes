import { Injectable } from '@nestjs/common';
import {User, UsersService} from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import {EncryptionHashingService} from "../encryption-hashing/encryption-hashing.service";

@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService,
        private jwtService: JwtService,
        private encryptionHashingService: EncryptionHashingService
    ) {}

    async validateUser(username: string, pass: string): Promise<User> {
        const user = await this.usersService.findOne(username);
        const isVerified = await this.encryptionHashingService.verifyHash(pass, user.passwordHash)
        if (user && isVerified) {
            const { passwordHash, ...result } = user;
            return result;
        }
        return null;
    }

    async login(user: User) {
        const payload = { username: user.username, sub: user.userId, roles: user.roles, permissions: user.permissions };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
}
