import {Module} from '@nestjs/common';
import {AuthService} from './auth.service';
import {LocalStrategy} from './local.strategy';
import {JwtStrategy} from './jwt.strategy';
import {UsersModule} from '../users/users.module';
import {PassportModule} from '@nestjs/passport';
import {JwtModule} from '@nestjs/jwt';
import {jwtConstants} from './constants';
import {EncryptionHashingModule} from "../encryption-hashing/encryption-hashing.module";

@Module({
    imports: [
        UsersModule,
        PassportModule,
        EncryptionHashingModule,
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: {expiresIn: '60m'},
        }),
    ],
    providers: [AuthService, LocalStrategy, JwtStrategy],
    exports: [AuthService],
})
export class AuthModule {
}
