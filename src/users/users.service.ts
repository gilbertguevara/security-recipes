import { Injectable } from '@nestjs/common';
import {Role} from "../enums/role.enum";
import {Permission} from "../enums/permission.enum";

export interface User {
    userId: number,
    username: string,
    passwordHash?: string,
    roles: Role[],
    permissions: Permission[]
}

@Injectable()
export class UsersService {
    private readonly users = [
        {
            userId: 1,
            username: 'john',
            passwordHash: '$2b$10$syRCCOTpymkSOn1TgbHm/elnhbrbABrsiZbT9QhpFwTSU8irYLPbu',
            roles: [Role.User],
            permissions: [Permission.DeleteArticle]
        },
        {
            userId: 2,
            username: 'jane',
            passwordHash: '$2b$10$rv.nfKl4rAj.5HhAULZIHO/Kia/MDenvNAUQze6Vc93k7gTfHIpXG',
            roles: [Role.Author],
            permissions: []
        },
    ];

    async findOne(username: string): Promise<User | undefined> {
        return this.users.find(user => user.username === username);
    }
}
